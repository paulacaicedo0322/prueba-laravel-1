<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;

class UsersController extends Controller
{
    public function index(){
        $users = User::all();

        return view('pages.users.index')->with('users', $users);
    }
    /**
     * Show the form for creating the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('pages.users.create')->with('roles', $roles);
    }

    /**
     * Store the newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'email' => 'email|required|unique:users|max:255',
            'name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'role' => 'required',
        ]);

        $user = new User;
        $user->name         = $request->name;
        $user->last_name    = $request->last_name;
        $user->phone        = $request->phone;
        $user->email        = $request->email;

        $user->save();

        if($request->role != null){
            $user->roles()->attach($request->role);
            $user->save();
        }
        $roles = Role::where('id', $request->role)->first();
        $permission = $roles->permissions()->get();

        if($permission != null){
            foreach($permission as $permission){
                $user->permissions()->attach($permission);
                $user->save();
            }
        }
        return redirect('/users-list');

    }

    /**
     * Display the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function permission_show($role_id)
    {
        $role = Role::where('id', $role_id)->first();
        $permissions = $role->permissions()->get();

        return view('pages.users.permissions', compact('role', 'permissions'));
    }

    public function consults()
    {
        return view('pages.consults.index');
    }

    // funcion que retorna el resultado de la consulta 1, usuarios con roles 1 y 2
    public function consults_one()
    {
        $relationRole = 'roles'; //Nombre de tu relacion con roles en el modelo User

        $usersConsult = User::whereHas($relationRole, function ($query) {
            return $query->whereIn('id',  [1,2]);
        })->get();

        $roles = Role::whereIn('id', [1,2])->get();

        return view('pages.consults.consult_one', compact('usersConsult', 'roles'));

    }

    // funcion que retorna el resultado de la consulta 2 (permisos del rol 1)
    public function consults_two()
    {
        $role = Role::where('id', 1)->first();
        $permissions = $role->permissions()->get();

        return view('pages.consults.consult_two', compact('permissions', 'role'));

    }

    // funcion que retorna el resultado de la consulta 3 (Los usuarios y el rol que tienen el permiso 2.)
    public function consults_three()
    {
        $relationPermissions = 'permissions'; 

        $usersConsult = User::whereHas($relationPermissions, function ($query) {
            return $query->whereIn('id',  [2]);
        })->get();

        $roleConsult = Role::whereHas($relationPermissions, function ($query) {
            return $query->whereIn('id',  [2]);
        })->get();

        return view('pages.consults.consult_three', compact('usersConsult', 'roleConsult'));
    }
}
