<header>
    <ul class="nav nav-pills nav-fill gap-2 p-1 small bg-primary rounded-5 shadow-sm" id="pillNav2" role="tablist" style="--bs-nav-link-color: var(--bs-white); --bs-nav-pills-link-active-color: var(--bs-primary); --bs-nav-pills-link-active-bg: var(--bs-white);">
        <li class="nav-item" role="presentation">
            <a href="/" class="nav-link  rounded-5" >Inicio</a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="{{route('users.index')}}" class="nav-link rounded-5" id="profile-tab2" >Usuarios</a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="{{route('consults')}}" class="nav-link rounded-5" id="contact-tab2"  type="button" role="tab" >Consultas</a>
        </li>
    </ul>
</header>