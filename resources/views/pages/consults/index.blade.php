<!DOCTYPE html>
<html lang="en">
@include('includes/head')
<body>
@include('includes/header')
   
</body>
<h1 class="display-3 text-center">Consultas</h1>
<br>
<div class="container border">
    <br>
    <div class="card">
        <div class="row">
            <div class="col-sm-4">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Consulta 1</h5>
                  <p class="card-text">Los usuarios que tengan el rol 1 y 2.</p>
                  <a href="{{route('consult.one')}}" class="btn btn-primary">Ver resultado</a>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Consulta 2</h5>
                  <p class="card-text">Los permisos que se tienen del rol 1.</p>
                  <a href="{{route('consult.two')}}" class="btn btn-primary">Ver resultado</a>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title">Consulta 3</h5>
                    <p class="card-text">Los usuarios y el rol que tienen el permiso 2.</p>
                    <a href="{{route('consult.three')}}" class="btn btn-primary">Ver resultado</a>
                  </div>
                </div>
              </div>
          </div>
    </div>
      <br>
</div>


@include('includes/footer')

</html>