<!DOCTYPE html>
<html lang="en">
@include('includes/head')
<body>
@include('includes/header')
   
</body>
<h1 class="display-3 text-center">Consulta dos</h1>
<p class=" text-center">Los permisos que se tienen del rol 1.</p>
<br>
<div class="container border" style="width: 50%">
    <br>
    <div class="card">
        <div class="row">
            <div class="col-sm-4">
                <div class="card">
                  <div class="card-body">
                        <h5 class="card-title">Rol 1: <span style="color:#0D6EFD">{{$role->name}}</span></h5>
                  </div>
                </div>
              </div>
            <div class="col-sm-8">
              <div class="card">
                <div class="card-body">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre del permiso</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($permissions as $permission)
                        <tr>
                            <th scope="row">{{$permission->id}}</th>
                            <td>{{$permission->name}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                      </table>
                </div>
              </div>
            </div>
            
          </div>
    </div>
      <br>
</div>


@include('includes/footer')

</html>