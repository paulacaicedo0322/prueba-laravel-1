<!DOCTYPE html>
<html lang="en">
@include('includes/head')
<body>
@include('includes/header')
   
</body>
<h1 class="display-3 text-center">Consulta tres</h1>
<p class=" text-center">Los usuarios y el rol que tienen el permiso 2.</p>
<br>
<div class="container border">
    <br>
    <div class="card">
        <div class="row">
            <div class="col-sm-4">
                <div class="card">
                  <div class="card-body">
                      @foreach($roleConsult as $role)
                              <h5 class="card-title">Rol {{$role->id}}: <span style="color:#0D6EFD">{{$role->name}}</span></h5>
                      @endforeach
                  </div>
                </div>
              </div>
            <div class="col-sm-8">
              <div class="card">
                <div class="card-body">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellido</th>
                            <th scope="col">Correo</th>
                            <th scope="col">Rol</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($usersConsult as $usuario)
                        <tr>
                            <th scope="row">{{$usuario->id}}</th>
                            <td>{{$usuario->name}}</td>
                            <td>{{$usuario->last_name}}</td>
                            <td>{{$usuario->email}}</td>  
                            <td>
                                @if ($usuario->roles())
                                    @foreach ($usuario->roles as $role)
                                    {{$role->name}}
                                    @endforeach
                                @endif
                            </td>  
                        </tr>
                        @endforeach
                        </tbody>
                      </table>
                </div>
              </div>
            </div>
            
          </div>
    </div>
      <br>
</div>


@include('includes/footer')

</html>