<!DOCTYPE html>
<html lang="en">
@include('includes/head')
<body>
@include('includes/header')
   
</body>
<br>
<h1 class="display-5 text-center">Usuarios</h1>
<br>
<div class="container border">
    <br>
    <form action="{{route('user.store')}}" method="post">
        @csrf
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <label for="" class="label-control">Nombre</label>
                    <input type="text" name="name" class="form-control" placeholder="Nombre" required>
                </div>
                <div class="col-4">
                    <label for="" class="label-control">Apellido</label>
                    <input type="text" name="last_name" class="form-control" placeholder="Apellido" required>
                </div>
                <div class="col-4">
                    <label for="" class="label-control">Celular</label>
                    <input type="text" name="phone" class="form-control" placeholder="Telefono" required>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-6"> 
                    <label for="" class="label-control">Correo</label>
                    <input type="email" name="email" class="form-control" placeholder="Correo" required>
                </div>
                <div class="col-6">
                    <label for="" class="label-control">Rol</label>
                    <select name="role" class="form-control" required>
                        @foreach ($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                </div>

            </div> 
            <br>
            <div class="col-12 text-center">
                <button class="btn btn-success btn-lg btn-block" type="submit">Guardar</button>
            </div>
        </div> 
        
    </form>
    
</div>

@include('includes/footer')

</html>