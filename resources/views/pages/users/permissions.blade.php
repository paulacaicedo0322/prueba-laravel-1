<!DOCTYPE html>
<html lang="en">
@include('includes/head')
<body>
@include('includes/header')
   
</body>
<br>
<h1 class="display-5 text-center">Permisos {{$role->name}}</h1>
<br>
<div class="container border" style="width:20%">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($permissions as $permission)
            <tr>
                <th scope="row">{{$permission->id}}</th>
                <td>{{$permission->name}}</td>
            <tr>
            @endforeach
        </tbody>
      </table>
</div>

@include('includes/footer')

</html>