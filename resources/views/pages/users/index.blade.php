<!DOCTYPE html>
<html lang="en">
@include('includes/head')
<body>
@include('includes/header')
   
</body>
<h1 class="display-3 text-center">Usuarios</h1>
<br>
<div class="container">
    <a href="{{route('user.create')}}" class="btn btn-primary cursor-point">Crear usuario</a>
</div>
<br>
<div class="container border" style="width:100%">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido</th>
            <th scope="col">Correo</th>
            <th scope="col">Rol</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($users as $usuario)
        <tr>
            <th scope="row">{{$usuario->id}}</th>
            <td>{{$usuario->name}}</td>
            <td>{{$usuario->last_name}}</td>
            <td>{{$usuario->email}}</td>  
            <td>
                @if ($usuario->roles())
                    @foreach ($usuario->roles as $role)
                    {{$role->name}}
                    <a href="{{route('permission.show', $role->id)}}" class="btn btn-warning " style="border-radius: 15%">Ver permisos</a>  
                    @endforeach
                @endif
            </td>  
        </tr>
        @endforeach
        </tbody>
      </table>
</div>

@include('includes/footer')

</html>