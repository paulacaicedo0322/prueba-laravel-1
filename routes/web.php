<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// rutas usuarios sistema de roles 
Route::get('/users-list', [App\Http\Controllers\UsersController::class, 'index'])->name('users.index');
Route::get('/user-create', [App\Http\Controllers\UsersController::class, 'create'])->name('user.create');
Route::post('/user-store', [App\Http\Controllers\UsersController::class, 'store'])->name('user.store');
Route::get('/user-create/{id}', [App\Http\Controllers\UsersController::class, 'permission_show'])->name('permission.show');

// rutas consultas punto 2 
Route::get('/consults', [App\Http\Controllers\UsersController::class, 'consults'])->name('consults');
Route::get('/consults-one', [App\Http\Controllers\UsersController::class, 'consults_one'])->name('consult.one');
Route::get('/consults-two', [App\Http\Controllers\UsersController::class, 'consults_two'])->name('consult.two');
Route::get('/consults-three', [App\Http\Controllers\UsersController::class, 'consults_three'])->name('consult.three');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
